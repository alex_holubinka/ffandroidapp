package com.alex.findfriends_api16.Connections;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

//import com.alex.findfriends_api16.UserData.CUserTable;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.in;

/**
 * Created by Alex on 24.07.2016.
 */
public class CServerConnection extends AsyncTask<String, Void, CResult> {
    //String mServerURL = "http://192.168.0.105/TestARESTcopy/public";
    String mServerURL = "http://192.168.0.103/FFlaravel/public/api";
    String mXDebugCode = "?XDEBUG_SESSION_START=12707";
    String mURL;
    CRequest mCRequest;
    CResult mCResult;
    CAuth mAuth;

    //FMethod mGetOrPostFlag;
    public enum FMethod {
        GET,
        POST
    }
    public enum AuthResult {
        OK,
        ERROR,
        REG_OK
    }
    public FMethod mMethod;
    public Type mResponseType;
    private final TaskListener taskListener;

    public interface TaskListener {
        public void onFinished(CResult result);
    }

    // This is the reference to the associated listener


    //public CServerConnection(TaskListener listener, CRequest request, CResult result) {
    public CServerConnection(TaskListener listener, CRequest request) {
        // The listener reference is passed in through the constructor
        this.taskListener = listener;
        this.mCRequest = request;
        //this.mCResult = result;
        this.mCResult = new CResult();
        this.mAuth = new CAuth();
    }
/*
    public void NewClient(){
        this.mURL = mServerURL + "/NewClient/?XDEBUG_SESSION_START=11268";
        mGetOrPostFlag = FMethod.POST;
        this.execute("asa");
    }
    public void ValideteClient(){
        this.mURL = mServerURL + "/ValidateClient/?XDEBUG_SESSION_START=11268";
        mGetOrPostFlag = FMethod.POST;
        this.execute("asa");
    }
    */

    public void Exec() {
        this.mURL = mServerURL + mCRequest.mMethodName + mXDebugCode;
        this.execute("");
    }

    @Override
    protected void onPreExecute() {

    }

    @Override
    protected CResult doInBackground(String... params) {
        String ServerResponse = "";
        switch (mMethod) {
            case POST:

                    try {
                       // HttpURLConnection connection = mAuth.connection;
                        URL url = new URL(this.mURL);
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        // connection = mAuth.getAuthorizedConnection(connection);
                        connection.setDoOutput(true);
                        connection.setDoInput(true);
                        connection.setRequestMethod("POST");
                        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

                        AuthResult authRes = mAuth.getAuthorizedConnection(connection, mServerURL, mCRequest.mMethodName, mXDebugCode);
                        if (authRes == AuthResult.OK) {
                            connection.disconnect();

                            URL f_url = new URL(this.mURL);
                            HttpURLConnection f_connection = (HttpURLConnection) url.openConnection();
                            // connection = mAuth.getAuthorizedConnection(connection);
                            f_connection.setDoOutput(true);
                            f_connection.setDoInput(true);
                            f_connection.setRequestMethod("POST");
                            f_connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

                            f_connection.connect();
                            Gson gson = new Gson();
                            String request = gson.toJson(mCRequest.ObjListRequest.get(0));
                            //String request = gson.toJson(mCRequest.ObjListRequest);
                            try {
                                JSONObject json = new JSONObject(request);
                                OutputStream out = f_connection.getOutputStream();
                                // DataOutputStream out = new DataOutputStream(connection.getOutputStream());
                                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(out, "UTF-8"));

                                writer.write(json.toString());
                                writer.flush();
                                writer.close();
                                out.close();
                                //Get Response
                                InputStream is = f_connection.getInputStream();
                                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                                // mCResult.ObjListResult = new Gson().fromJson(rd, mResponseType);
                                StringBuffer response = new StringBuffer();
                                while ((ServerResponse = rd.readLine()) != null) {
                                    response.append(ServerResponse);
                                    //response.append('\r');
                                }
                                mCResult.SERVER_RESPONSE = response.toString();
                                rd.close();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if(authRes == AuthResult.REG_OK){
                            mCResult.SERVER_RESPONSE = "OK";
                        } else if(authRes == AuthResult.ERROR){
                            mCResult.SERVER_RESPONSE = "ERROR";
                        }
                    } catch (MalformedURLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {

                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                break;
            case GET:
                try {
                    BufferedReader reader = null;
                    String resultJson = "";
                    URL url = new URL(this.mURL);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoOutput(false);
                    connection.setDoInput(true);
                    connection.setRequestMethod("GET");//
                    connection.connect();


                    if (!mCRequest.ObjListRequest.isEmpty()) {
                        Gson gson = new Gson();
                        String request = gson.toJson(mCRequest.ObjListRequest.get(0));
                        JSONObject json = new JSONObject(request);
                        OutputStream out = connection.getOutputStream();

                        BufferedWriter writer = new BufferedWriter(
                                new OutputStreamWriter(out, "UTF-8"));

                        writer.write(json.toString());
                        writer.flush();
                        writer.close();
                        out.close();
                    }
                    InputStream inputStream = connection.getInputStream();

                    reader = new BufferedReader(new InputStreamReader(inputStream));

                    mCResult.ObjListResult = new Gson().fromJson(reader, mResponseType);

                } catch (MalformedURLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (JSONException e) {
            e.printStackTrace();
        }
                break;

        }
        return mCResult;
    }
    protected void onPostExecute(CResult mCResult) {
        super.onPostExecute(mCResult);
        // In onPostExecute we check if the listener is valid
        if(this.taskListener != null) {

            // And if it is we call the callback function on it.
            this.taskListener.onFinished(mCResult);
        }
    }
}
/*    protected void onPostExecute(String ServerResponse) {
        super.onPostExecute(ServerResponse);
        // In onPostExecute we check if the listener is valid
        if(this.taskListener != null) {

            // And if it is we call the callback function on it.
            this.taskListener.onFinished(ServerResponse);
        }
    }
}*/
