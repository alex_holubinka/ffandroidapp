package com.alex.findfriends_api16.Connections;

/**
 * Created by Alex on 22.12.2016.
 */
import com.alex.findfriends_api16.UserData.CUserTable;
import com.alex.findfriends_api16.UserData.JWTErrors;
import com.alex.findfriends_api16.UserData.JWTToken;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CAuth {
    String mToken;
    String mServerURL;
    String mMethodName;
    String mXDebugCode;
    public Type mResponseType;
    String mURL;
    public HttpURLConnection connection = null;
    CServerConnection.AuthResult authRezult;

    public CServerConnection.AuthResult getAuthorizedConnection(HttpURLConnection conn, String server , String method, String xdebug){
        this.connection = conn;
        this.mMethodName = method;
        this.mServerURL = server;
        this.mXDebugCode = xdebug;

        if (method == "/autentification"){
            authRezult = autentification();
        }else{
            authRezult = authorizationRequest();
        }
        return authRezult;
    }

    public CServerConnection.AuthResult autentification(){
        try{
            connection.connect();
            Gson gson = new Gson();
            String request = gson.toJson(CUserTable.getInstance());
            this.mResponseType = new TypeToken<JWTErrors>() {}.getType();

            try {
                JSONObject json = new JSONObject(request);
                OutputStream out = connection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(out, "UTF-8"));
                writer.write(json.toString());
                writer.flush();
                writer.close();
                out.close();
                //Get Response
                InputStream isEror = connection.getErrorStream();
                if (isEror == null) {
                    mResponseType = new TypeToken<JWTToken>() {}.getType();
                    InputStream is = connection.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    JWTToken fToken = new JWTToken();
                    fToken = new Gson().fromJson(reader, mResponseType);
                    CUserTable.getInstance().mToken = fToken.jwtToken.toString();
                    connection.disconnect();
                    authRezult = CServerConnection.AuthResult.REG_OK;
                } else {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(isEror));
                    JWTErrors mError = new Gson().fromJson(reader, mResponseType);
                    if (mError.jwtError.equals("token_not_provided") ) {
                        authRezult = CServerConnection.AuthResult.ERROR;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return authRezult;
    }


    public CServerConnection.AuthResult authorizationRequest(){

        mResponseType = new TypeToken<JWTErrors>() {}.getType();
        connection.setRequestProperty("Authorization","Bearer " + CUserTable.getInstance().mToken);
        try {
            //Get Response
           InputStream is = connection.getErrorStream();
            if (is == null) {
                connection.disconnect();
                authRezult = CServerConnection.AuthResult.OK;
            } else {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                JWTErrors fError = new JWTErrors();
                fError = new Gson().fromJson(reader, mResponseType);

                switch (fError.jwtError){
                    case "token_not_provided" :
                        return CServerConnection.AuthResult.ERROR;
                    case "token_invalid" :
                        SignIn();
                        break;
                    case "token_expired" :
                        RefreshToken();
                        break;
                    default:
                        return CServerConnection.AuthResult.ERROR;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return authRezult;
    }
    private CServerConnection.AuthResult SignIn(){
        try{
            this.mURL = mServerURL + "signin" + mXDebugCode;
            URL url = new URL(this.mURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            // connection = mAuth.getAuthorizedConnection(connection);
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            connection.connect();
            Gson gson = new Gson();
            String request = gson.toJson(CUserTable.getInstance());
            mResponseType = new TypeToken<JWTErrors>() {}.getType();
            try {
                JSONObject json = new JSONObject(request);
                OutputStream out = connection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(out, "UTF-8"));
                writer.write(json.toString());
                writer.flush();
                writer.close();
                out.close();
                //Get Response
                InputStream is = connection.getErrorStream();
                if (is == null) {
                    is = connection.getInputStream();
                } else {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    JWTErrors mError = new Gson().fromJson(reader, mResponseType);
                    if (mError.jwtError.equals("token_not_provided") ) {
                        return CServerConnection.AuthResult.ERROR;
                    }
                }
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                StringBuffer response = new StringBuffer();
                String ServerResponse;
                while ((ServerResponse = rd.readLine()) != null) {
                    response.append(ServerResponse);
                }
                CUserTable.getInstance().mToken = response.toString();
                rd.close();
                return CServerConnection.AuthResult.REG_OK;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return CServerConnection.AuthResult.ERROR;
    }

    private CServerConnection.AuthResult RefreshToken(){
        try{
            this.mURL = mServerURL + "refreshToken" + mXDebugCode;
            URL url = new URL(this.mURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            // connection = mAuth.getAuthorizedConnection(connection);
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            connection.connect();
            Gson gson = new Gson();
            String request = gson.toJson(CUserTable.getInstance());
            mResponseType = new TypeToken<List<JWTErrors>>() {}.getType();
            try {
                JSONObject json = new JSONObject(request);
                OutputStream out = connection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(out, "UTF-8"));
                writer.write(json.toString());
                writer.flush();
                writer.close();
                out.close();
                //Get Response
                InputStream is = connection.getErrorStream();
                if (is == null) {
                    is = connection.getInputStream();
                } else {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    JWTErrors mError = new Gson().fromJson(reader, mResponseType);
                    if (mError.jwtError.equals("token_not_provided") ) {
                        return CServerConnection.AuthResult.ERROR;
                    }
                }
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                StringBuffer response = new StringBuffer();
                String ServerResponse;
                while ((ServerResponse = rd.readLine()) != null) {
                    response.append(ServerResponse);
                }
                CUserTable.getInstance().mToken = response.toString();
                rd.close();
                return CServerConnection.AuthResult.REG_OK;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return CServerConnection.AuthResult.ERROR;
    }

}
