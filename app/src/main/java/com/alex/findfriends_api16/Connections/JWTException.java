package com.alex.findfriends_api16.Connections;

/**
 * Created by Alex on 23.12.2016.
 */

public class JWTException extends Exception {

    public JWTException() { super(); }
    public JWTException(String message) { super(message); }
    public JWTException(String message, Throwable cause) { super(message, cause); }
    public JWTException(Throwable cause) { super(cause); }
}
