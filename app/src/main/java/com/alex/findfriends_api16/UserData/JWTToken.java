package com.alex.findfriends_api16.UserData;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 01.02.2017.
 */

public class JWTToken {
    @SerializedName("token")
    public String jwtToken;
}
