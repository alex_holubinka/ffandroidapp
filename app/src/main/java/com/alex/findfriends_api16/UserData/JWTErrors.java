package com.alex.findfriends_api16.UserData;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 26.01.2017.
 */

public class JWTErrors {
    @SerializedName("error")
    public String jwtError;
}
