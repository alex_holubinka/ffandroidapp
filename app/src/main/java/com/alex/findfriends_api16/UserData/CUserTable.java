package com.alex.findfriends_api16.UserData;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 17.07.2016.
 */
public class CUserTable {
    private static CUserTable instance;


    public static CUserTable getInstance() {
        if(instance == null){
            instance = new CUserTable();
        }
        return instance;
    }

    public CUserTable() {
        mIdff_users = null;
        mId_record = null;
        mPhone = null;
        mName = null;
        mPassword = null;
        mPhoto = null;
        mValidate = null;
    }
    public void Copy(CUserTable other){
        mIdff_users = other.mIdff_users;
        mId_record = other.mId_record;
        mPhone = other.mPhone;
        mName = other.mName;
        mPassword = other.mPassword;
        mPhoto = other.mPhoto;
        mValidate = other.mValidate;
    }

    @SerializedName("idff_users")
    public Integer mIdff_users;

    @SerializedName("id_record")
    public Integer mId_record;

    @SerializedName("phone")
    public String mPhone;

    @SerializedName("name")
    public String mName;

    @SerializedName("password")
   public String mPassword;

    @SerializedName("photo")
    public String mPhoto;

    @SerializedName("validate")
    public Boolean mValidate;

    @SerializedName("Contacts")
    public List<CUserTable> mContacts;

    public boolean mIsRegistred;

    @SerializedName("ff_isfriend")
    public boolean mIsFriend;

    @SerializedName("latitude")
    public double mLatitude;

    @SerializedName("longitude")
    public double mLongitude;

    public String mToken;
}
