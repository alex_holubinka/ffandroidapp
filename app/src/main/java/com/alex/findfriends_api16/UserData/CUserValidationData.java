package com.alex.findfriends_api16.UserData;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 11.08.2016.
 */
public class CUserValidationData {

    @SerializedName("phone")
    public String mPhone;

    @SerializedName("validcode")
    public String mValidcode;

}
