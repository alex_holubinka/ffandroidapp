package com.alex.findfriends_api16.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.alex.findfriends_api16.Models.ContactsModel;
import com.alex.findfriends_api16.R;
import com.alex.findfriends_api16.UserData.CUserTable;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class ContactsActivity extends AppCompatActivity implements Observer {
    private ListView mListView;
    private ProgressDialog pDialog;
    private Handler updateBarHandler;
    private ContactsModel mContactsModel;

    Cursor cursor;
    int counter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
       // mContactsModel = new ContactsModel(ContactsActivity.this);
        mContactsModel.addObserver(this);
        try {
            //mContactsModel.getContacts();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void update(Observable observable, Object reason) {
        String fReason = (String)reason;

        if (fReason.equals("contactList")) {
            mContactsModel.GetRegisteredContacts();

        }

    }
        /*
        ArrayList<String> contactList;
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Reading contacts...");
        pDialog.setCancelable(false);
        pDialog.show();
        mListView = (ListView) findViewById(R.id.list);
        updateBarHandler =new Handler();
        // Since reading contacts takes more time, let's run it on a separate thread.
        new Thread(new Runnable() {
            @Override
            public void run() {
                getContacts();
            }
        }).start();
        // Set onclicklistener to the list item.
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //TODO Do whatever you want with the list data
                Toast.makeText(getApplicationContext(), R.string.itemClicked + " : \n"+contactList.get(position), Toast.LENGTH_SHORT).show();
            }
        });*/


           /* // ListView has to be updated using a ui thread
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_item, R.id.text1, contactList);
                    mListView.setAdapter(adapter);
                }
            });
            // Dismiss the progressbar after 500 millisecondds
            updateBarHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    pDialog.cancel();
                }
            }, 500);
        }
    }*/
     /*      public void getContacts(Context context) {
               contactList = new ArrayList<CUserTable>();
               String phoneNumber = null;
               String email = null;
               Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
               String _ID = ContactsContract.Contacts._ID;
               String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
               String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
               Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
               String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
               String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
               Uri EmailCONTENT_URI =  ContactsContract.CommonDataKinds.Email.CONTENT_URI;
               String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
               String DATA = ContactsContract.CommonDataKinds.Email.DATA;
               StringBuffer output;
               ContentResolver contentResolver = context.getContentResolver();
               cursor = contentResolver.query(CONTENT_URI, null,null, null, null);
               // Iterate every contact in the phone
               if (cursor.getCount() > 0) {
                   counter = 0;
                   while (cursor.moveToNext()) {
                       output = new StringBuffer();
                       // Update the progress message
                       updateBarHandler.post(new Runnable() {
                           public void run() {
                                pDialog.setMessage(getString(R.string.readingContacts) + " : "+ counter++ +"/"+cursor.getCount());
                           }
                       });
                       String contact_id = cursor.getString(cursor.getColumnIndex( _ID ));
                       String name = cursor.getString(cursor.getColumnIndex( DISPLAY_NAME ));
                       int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex( HAS_PHONE_NUMBER )));
                       if (hasPhoneNumber > 0) {
                           //output.append("\n" + getString(R.string.phoneNumberUserName) + ":" + name);
                           //This is to read multiple phone numbers associated with the same contact
                           Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[] { contact_id }, null);
                           while (phoneCursor.moveToNext()) {
                               phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                                output.append("\n" + getString(R.string.phoneNumber) + ":" + phoneNumber);
                           }
                           phoneCursor.close();
                           // Read every email id associated with the contact
                           Cursor emailCursor = contentResolver.query(EmailCONTENT_URI,    null, EmailCONTACT_ID+ " = ?", new String[] { contact_id }, null);
                           while (emailCursor.moveToNext()) {
                               email = emailCursor.getString(emailCursor.getColumnIndex(DATA));
                               output.append("\n Email:" + email);
                           }
                           emailCursor.close();
                       }
                       // Add the contact to the ArrayList
                       if(hasPhoneNumber != 0){
                            contactList.add(output.toString());
                       }
                   }*/
}
