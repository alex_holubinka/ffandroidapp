package com.alex.findfriends_api16.Activity.NavigationActivityFragments.Contacts;


import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;


import com.alex.findfriends_api16.Activity.NavigationActivity;
import com.alex.findfriends_api16.Activity.NavigationActivityFragments.Contacts.Adapters.DetailedContactsListAdapter;
import com.alex.findfriends_api16.R;
import com.alex.findfriends_api16.UserData.CUserTable;


public class DetailListActivity extends AppCompatActivity {
    private int position;
    Toolbar mActionBar;
    ListView mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_list);
        Intent intent = getIntent();
        position = intent.getIntExtra("position", 1);
        DetailedContactsListAdapter detailedListAdapter = new DetailedContactsListAdapter(getApplicationContext(), CUserTable.getInstance(), position);
        mList = (ListView) findViewById(R.id.detail_list);
        mList.setAdapter(detailedListAdapter);

        // Show the Up button in the action bar.
        //mActionBar = (Toolbar)findViewById(R.id.detail_toolbar);
        // Show the Up button in the action bar.
        android.support.v7.app.ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpTo(this, new Intent(this, ContactFragment.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {


        NavUtils.navigateUpTo(this, new Intent(this, ContactFragment.class));
        this.finish();
    }
}
