package com.alex.findfriends_api16.Activity.NavigationActivityFragments.Contacts.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.alex.findfriends_api16.R;
import com.alex.findfriends_api16.UserData.CUserTable;


/**
 * Created by User on 14.10.2016.
 */

public class ContactsListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private CUserTable mUserTable;


    private final static int[] images = {R.drawable.logo};
    public ContactsListAdapter(Context context, CUserTable UserTable){
        super();
        this.mUserTable = UserTable;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public int getCount() {
       return mUserTable.mContacts.size();
        //return contacts.length;
    }

    @Override
    public Object getItem(int position) {
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = inflater.inflate(R.layout.row, null);

        if (mUserTable.mContacts.get(position).mValidate != null) {
            if (mUserTable.mContacts.get(position).mValidate) {
                ImageView image = (ImageView) view.findViewById(R.id.option_icon);
                image.setImageResource(images[0]);
            }
        }
        /*if(imageTrue[position]){
            ImageView image = (ImageView) view.findViewById(R.id.option_icon);

            image.setImageResource(images[position]);
        }*/
        TextView  text = (TextView)view.findViewById(R.id.option_text);
        text.setText(mUserTable.mContacts.get(position).mName);
        return view;
    }
}
