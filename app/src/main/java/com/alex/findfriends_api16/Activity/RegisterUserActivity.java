package com.alex.findfriends_api16.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.alex.findfriends_api16.Models.RegisterUserModel;
import com.alex.findfriends_api16.R;
import com.alex.findfriends_api16.UserData.CUserTable;

import java.util.Observable;
import java.util.Observer;

public class RegisterUserActivity extends AppCompatActivity implements Observer{
    public final static String KEY_REGISTER_RESULT = "com.alex.findfriends.REGISTERRESULT";
    //public final static String KEY_LOGIN_USERNAME = "com.alex.findfriends.KEYLOGINUSERNAME";
    //public final static String KEY_LOGIN_PHONE = "com.alex.findfriends.KEYLOGINPHONE";
    EditText mLblPhone;
    EditText mLblName;
    EditText mLblPassword;
    private RegisterUserModel mRegisterUserModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);
        mRegisterUserModel = RegisterUserModel.getInstance(); //new RegisterUserModel();
        mRegisterUserModel.addObserver(this);
        Button mBtnRegister = (Button)findViewById(R.id.btnRegister);
        mLblPhone = (EditText)findViewById(R.id.lblPhone);
        mLblName = (EditText)findViewById(R.id.lblName);
        mLblPassword = (EditText)findViewById(R.id.lblPassword);

        mLblPhone.setText("+380679271623");
        mLblName.setText("alex");
        mLblPassword.setText("asas");;

       /*  android:focusableInTouchMode="true"
       mLblPhone.setOnTouchListener(new View.OnTouchListener()
        {
            public boolean onTouch(View arg0, MotionEvent arg1)
            {
                mLblPhone.getText().clear();
                return false;
            }
        });
        mLblName.setOnTouchListener(new View.OnTouchListener()
        {
            public boolean onTouch(View arg0, MotionEvent arg1)
            {
                mLblName.getText().clear();
                return false;
            }
        });*/
    }

    @Override
    public void update(Observable observable, Object reason) {
        String fReason = (String)reason;

        if (fReason.equals("mResultRegister")) {
            Intent answerIntent = new Intent();
            StopProgressDialog();
            if(mRegisterUserModel.mResultRegister.SERVER_RESPONSE.equals("ok")){
                CUserTable.getInstance().mPhone = mLblPhone.getText().toString();
                CUserTable.getInstance().mName = mLblName.getText().toString();
                CUserTable.getInstance().mPassword = mLblPassword.getText().toString();
            }
        /*else if(mRegisterUserModel.mResult.SERVER_RESPONSE.equals("user_duplicate")){

        }*/
            answerIntent.putExtra(KEY_REGISTER_RESULT,mRegisterUserModel.mResultRegister.SERVER_RESPONSE);
            setResult(RESULT_OK, answerIntent);
            mRegisterUserModel.deleteObserver(this);
            finish();
        }



    }

    public void onClickLblPhone(View view) {
       // mLblPhone.getText().clear();
    }



    public void onClickBtnRegister(View view) {
        //RunProgressDialog();
       // mRegisterUserModel.SendSMS();
        String phone = mLblPhone.getText().toString();
        String name = mLblName.getText().toString();
        String password = mLblPassword.getText().toString();

        //mRegisterUserModel.auth(mLblPhone.getText().toString(), mLblName.getText().toString());
        mRegisterUserModel.auth(phone, name, password);
    }


    private void RunProgressDialog(){
        ProgressDialog progDailog = new ProgressDialog(this);
        progDailog.setMessage("Loading...");
        progDailog.setIndeterminate(false);
        progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDailog.setCancelable(true);
        progDailog.show();
    }
    private void StopProgressDialog(){
       // progDailog.dismiss();
    }


    public void onClickLblName(View view) {
    }

    public void onClickLblPassword(View view) {
    }
}