package com.alex.findfriends_api16.Activity;





import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;


import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


import com.alex.findfriends_api16.Connections.CRequest;
import com.alex.findfriends_api16.Connections.CResult;
import com.alex.findfriends_api16.Connections.CServerConnection;
import com.alex.findfriends_api16.R;
import com.alex.findfriends_api16.UserData.CUserTable;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ProgressDialog progress;
    static final private int ACTIVITY_REGISTER_REQUEST_CODE =1;
    static final private int ACTIVITY_VALIDATION_REQUEST_CODE =2;
    private String mUserPhone;
    private String mUserName;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //GetContects();
       // GetClientData();
        //WriteToFileFlagIsUserRegistred(false);

        //Intent intent = new Intent(MainActivity.this,NavigationActivity.class);
        //startActivity(intent);

        //LogIn();
        //GetContects();
        if(!CheckFlagIsUserRegistredInFile()) {
            LoadRegisterActivity();
        }

       /* if (!CheckFlagIsUserValidatedInFile()) {
            LoadValidateRequestCodeActivity();
        }*/
    }

    @Override
    public void onRestart() {
        super.onRestart();
    }
    @Override
    public void onStart() {
        super.onStart();
    }
    @Override
    public void onResume() {
        super.onResume();
    }
    @Override
    public void onPause() {
        super.onPause();
    }
    @Override
    public void onStop() {
        super.onStop();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
//-----------------------------------------LOAD ACTIVITYS-------------------------------------------
    private void LoadRegisterActivity(){
        Intent intent = new Intent(MainActivity.this, RegisterUserActivity.class);
        startActivityForResult(intent, ACTIVITY_REGISTER_REQUEST_CODE);
    }
    private  void LoadValidateRequestCodeActivity(){
        Intent intent = new Intent(MainActivity.this, ValidateRequestCodeActivity.class);
        startActivityForResult(intent, ACTIVITY_VALIDATION_REQUEST_CODE);
    }
    private void LoadNavigationActivity(){
        Intent intent = new Intent(MainActivity.this,NavigationActivity.class);
        startActivity(intent);
    }
//========================================LOAD ACTIVITYS============================================



//-----------------------------------------USER FUNCTIONS-------------------------------------------

    private void GetContects(){
       CRequest mCRequest = new CRequest();
       CUserTable fCUserTable = new CUserTable();
       fCUserTable.mContacts = new ArrayList<CUserTable>();
       for(int i=1; i<6; i++){
           CUserTable contacts = new CUserTable();
           contacts.mPhone = "+38067000000" + i ;
           fCUserTable.mContacts.add(contacts);
       }
       fCUserTable.mIdff_users = 1;
       mCRequest.ObjListRequest.add(fCUserTable);
       mCRequest.mMethodName = "/getName";

       CServerConnection mCServerConnection = new CServerConnection(GetRegisteredContactsListener, mCRequest);
       mCServerConnection.mMethod = CServerConnection.FMethod.POST;
       mCServerConnection.mResponseType = new TypeToken<List<CUserTable>>() {}.getType();
       mCServerConnection.Exec();
   }
    CServerConnection.TaskListener GetRegisteredContactsListener = new CServerConnection.TaskListener() {
        @Override
        public void onFinished(CResult result){
            if(result.SERVER_RESPONSE.equals("ok")){
                //LoadValidateRequestCodeActivity();
            }
        }
    };
    /* private void RegisterNewUser (String userName, String userPhone) {
        CRequest mCRequest = new CRequest();
        CUserTable fCUserTable = new CUserTable();
        fCUserTable.mPhone = userPhone;
        fCUserTable.mName = userName;
        fCUserTable.mPassword = "findfriends123";
        fCUserTable.mValidate = "false";

        /*fCUserTable.mId_record = 1;  //temporary
        fCUserTable.mIdff_users = 1;  //temporary
        fCUserTable.mPhone = userPhone;
        fCUserTable.mName = userName;
        fCUserTable.mPassword = null;
        fCUserTable.mPhoto = null;
        fCUserTable.mValidate = null;


        mCRequest.ObjListRequest.add(fCUserTable);
        mCRequest.mMethodName = "/NewClient/?XDEBUG_SESSION_START=" + mXdebugCode;

        CServerConnection mCServerConnection = new CServerConnection(RegisterNewUserListener, mCRequest);
        mCServerConnection.mMethod = CServerConnection.FMethod.POST;
        mCServerConnection.mResponseType = new TypeToken<List<CUserTable>>() {}.getType();
        mCServerConnection.Exec();
    }*/

    /*private void ValidateUser(String validcode){
        CUserValidationData fCUserValidationData= new CUserValidationData();
        fCUserValidationData.mValidcode = validcode;
        fCUserValidationData.mPhone = mUserPhone; // треба подумати як зробити з номером
        CRequest mCRequest = new CRequest();
        mCRequest.mMethodName = "/ValidateClient/?XDEBUG_SESSION_START=" + mXdebugCode;
        mCRequest.ObjListRequest.add(fCUserValidationData);
        CServerConnection mCServerConnection = new CServerConnection(ValidateRequestCodeListener, mCRequest);
        mCServerConnection.mMethod = CServerConnection.FMethod.POST;
        mCServerConnection.mResponseType = new TypeToken<List<CUserTable>>() {}.getType();
       // mCServerConnection.ValideteClient();
        mCServerConnection.Exec();
    }*/
    private void LogIn(){
        CRequest mCRequest = new CRequest();
        CUserTable fCUserTable = new CUserTable();
        fCUserTable.mPhone = "+380679271623";//CUserTable.getInstance().mPhone;
        mCRequest.mMethodName = "/getName";
        mCRequest.ObjListRequest.add(fCUserTable);
        CServerConnection mCServerConnection = new CServerConnection(LogInListener, mCRequest);
        mCServerConnection.mMethod = CServerConnection.FMethod.POST;
        mCServerConnection.Exec();
    }
    /*private void GetClientData(){
        CRequest mCRequest = new CRequest();
        mCRequest.mMethodName = "/GetClientData/";
        CServerConnection mCServerConnection = new CServerConnection(GetUserDataListener, mCRequest);
        mCServerConnection.mMethod = CServerConnection.FMethod.GET;
        mCServerConnection.mResponseType = new TypeToken<List<CUserTable>>() {}.getType();
        mCServerConnection.Exec();
    }*/
//========================================USER FUNCTIONS============================================



//-----------------------------------ACTIVITIES RESULT-----------------------------------------------
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case ACTIVITY_REGISTER_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    String fResult = data.getStringExtra(RegisterUserActivity.KEY_REGISTER_RESULT);
                    //if(fResult.equals("OK") || fResult.equals("user_duplicate") ){
                    if(fResult.equals("OK")){
                       LoadValidateRequestCodeActivity();
                    }
                }
                break;
            case ACTIVITY_VALIDATION_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    String fResult = data.getStringExtra(ValidateRequestCodeActivity.KEY_VALIDATION_RESULT);
                    if(fResult.equals("ok")){

                    }
                }
                break;
        }
    }
//========================================ACTIVITIES RESULT==========================================



//--------------------------------------ASYNK LISTNERS---------------------------------------------
    /*CServerConnection.TaskListener RegisterNewUserListener = new CServerConnection.TaskListener() {
        @Override
        public void onFinished(CResult result){
            ;
            if(result.SERVER_RESPONSE.equals("ok")){
                LoadValidateRequestCodeActivity();
            }
        }
    };*/
    /*CServerConnection.TaskListener ValidateRequestCodeListener = new CServerConnection.TaskListener() {
        @Override
        public void onFinished(CResult result){
            ;
            if(result.SERVER_RESPONSE.equals("ok")){
                LogIn();
            }
        }
    };*/
    CServerConnection.TaskListener LogInListener = new CServerConnection.TaskListener() {
        @Override
        public void onFinished(CResult result){
            ;
            if(result.SERVER_RESPONSE.equals("ok")){
                LoadNavigationActivity();
            }
        }
    };
    /*CServerConnection.TaskListener GetUserDataListener = new CServerConnection.TaskListener() {
        @Override
        public void onFinished(CResult result){
            ;
            if(!result.ObjListResult.isEmpty()){
                CUserTable resUser = (CUserTable)result.ObjListResult.get(0);
                CUserTable selfUser = CUserTable.getInstance();
                selfUser.Copy(resUser);

            }
        }
    };*/
//========================================ASYNK LISTNERS===========================================



//------------------------------File Setting section-----------------------------------------------

    public Boolean CheckFlagIsUserRegistredInFile(){
        Context context = this;
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        if (sharedPref.contains(getString(R.string.IdFlagIsUserRegistred))){
            Boolean IdFlagIsUserRegistred = sharedPref.getBoolean(getString(R.string.IdFlagIsUserRegistred), false);
            return IdFlagIsUserRegistred;
        }
        return false;
    }
    public Boolean CheckFlagIsUserValidatedInFile(){
        Context context = this;
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        if (sharedPref.contains(getString(R.string.IdFlagIsUserValidated))){
            Boolean IdFlagIsUserRegistred = sharedPref.getBoolean(getString(R.string.IdFlagIsUserValidated), false);
            return IdFlagIsUserRegistred;
        }
        return false;
    }
    /*SharedPreferences sharedPref = context.getSharedPreferences(
            getString(R.string.preference_file_key), Context.MODE_PRIVATE);*/

    public void WriteSettingsToFile(String key, Boolean val){
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(getString(R.string.IdFlagIsUserRegistred) ,val);
        editor.commit();
    }
//========================================File Setting section======================================
    public void onClickBtnGetContacts(View view) {

        /*FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        ContactsFragment mContactsFragment = new ContactsFragment();
        fragmentTransaction.add(R.id.fragmentLayout, mContactsFragment);
        fragmentTransaction.commit();
        //Intent intent = new Intent(MainActivity.this, ContactsActivity.class);
        //startActivity(intent);*/
    }

    public void onClickBtnTest(View view) {

        /*FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        SMSFragment mSMSFragment = new SMSFragment();
        fragmentTransaction.add(R.id.fragmentLayout, mSMSFragment);
        fragmentTransaction.commit();
        //Intent intent = new Intent(Intent.ACTION_DIAL);
        //startActivity(intent);*/
    }

    public void onClickBtnExit(View view) {
       // Intent intent = new Intent(MainActivity.this, ExitFormActivity.class);
        //startActivity(intent);
    }
}