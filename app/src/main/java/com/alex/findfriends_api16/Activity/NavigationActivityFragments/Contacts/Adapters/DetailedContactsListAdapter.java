package com.alex.findfriends_api16.Activity.NavigationActivityFragments.Contacts.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.alex.findfriends_api16.R;
import com.alex.findfriends_api16.UserData.CUserTable;


/**
 * Created by User on 17.10.2016.
 */




    public class DetailedContactsListAdapter extends BaseAdapter {
        private LayoutInflater inflater;
        private CUserTable mUserTable;
        private int mIndx;


        public DetailedContactsListAdapter(Context context, CUserTable UserTable, int indx){
            super();
            this.mUserTable = UserTable;
            this.mIndx = indx;
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }
        @Override
        public int getCount() {
            return mUserTable.mContacts.get(mIndx).mContacts.size();
            //return contacts.length;
        }

        @Override
        public Object getItem(int position) {
            return 0;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View view = inflater.inflate(R.layout.row, null);


        /*if(imageTrue[position]){
            ImageView image = (ImageView) view.findViewById(R.id.option_icon);

            image.setImageResource(images[position]);
        }*/
            TextView text = (TextView)view.findViewById(R.id.option_text);

            text.setText(mUserTable.mContacts.get(mIndx).mContacts.get(position).mPhone.toString());

            return view;
        }


}
