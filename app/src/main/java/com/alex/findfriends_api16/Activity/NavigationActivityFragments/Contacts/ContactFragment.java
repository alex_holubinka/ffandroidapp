package com.alex.findfriends_api16.Activity.NavigationActivityFragments.Contacts;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.alex.findfriends_api16.Activity.NavigationActivityFragments.Contacts.Adapters.ContactsListAdapter;
import com.alex.findfriends_api16.Models.ContactsModel;
import com.alex.findfriends_api16.R;
import com.alex.findfriends_api16.UserData.CUserTable;

import java.util.Observable;
import java.util.Observer;

public class ContactFragment extends ListFragment implements Observer {
    private ContactsModel mContactsModel;
    ListView mListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_contact, container, false);
        mListView = (ListView)v.findViewById(android.R.id.list);

        return v;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContactsModel = new ContactsModel();
        mContactsModel.addObserver(this);

        try {
            mContactsModel.getContacts(getActivity().getContentResolver());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    public void update(Observable observable, Object reason) {
        String fReason = (String)reason;

        if (fReason.equals("getContacts")) {
            mContactsModel.GetRegisteredContacts();
        } else if (fReason.equals("GetRegisteredContactsListener")) {
            setListAdapter(new ContactsListAdapter(getActivity().getApplicationContext(), CUserTable.getInstance() ));


            //mContactsModel.GetContactListString();
        } else if (fReason.equals("GetContactListString")){
           // ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.list_item, R.id.text1, mContactsModel.mContactListView);
           // mListView.setAdapter(adapter);

            //mContactsAdapter = new CContactsAdapter(getActivity(),mContactsModel.mContactListView);

        }

    }

    @Override
    public void onListItemClick(ListView listView, View v, int position, long id){
        String selecteditem = getListView().getItemAtPosition(position).toString();


        Intent intent = new Intent(getActivity(), DetailListActivity.class);

        // Use TaskStackBuilder to build the back stack and get the PendingIntent
        PendingIntent pendingIntent =
                TaskStackBuilder.create(getActivity())
                        // add all of DetailsActivity's parents to the stack,
                        // followed by DetailsActivity itself
                        .addNextIntentWithParentStack(intent)
                        .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getActivity());
        builder.setContentIntent(pendingIntent);

        intent.putExtra("position", position);
        getActivity().startActivity(intent);

        /*detailedContactsFragment.setArguments(bundle);
        this.getFragmentManager().beginTransaction()
                .replace(R.id.fragmentLayout, detailedContactsFragment)
                .addToBackStack("tag")
                .commit();*/
        //text.setText("Detailed:\n\n\t" + selecteditem + "Phone:\n\n\t" + CUserTable.getInstance().mContacts.get(position).mPhone.toString() );
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        mContactsModel.deleteObserver(this);
    }
}
