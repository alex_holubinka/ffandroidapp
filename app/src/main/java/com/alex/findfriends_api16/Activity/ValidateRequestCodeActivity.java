package com.alex.findfriends_api16.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.alex.findfriends_api16.Models.RegisterUserModel;
import com.alex.findfriends_api16.Models.ValidateRequestCodeModel;
import com.alex.findfriends_api16.R;
import com.alex.findfriends_api16.UserData.CUserTable;

import java.util.Observable;
import java.util.Observer;

public class ValidateRequestCodeActivity extends AppCompatActivity implements Observer {
    public final static String KEY_VALIDATION_RESULT = "com.alex.findfriends.KEYVALIDATIONNUMBER";
    private RegisterUserModel mRegisterUserModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate_request_code);
        Button mBtnValidate = (Button)findViewById(R.id.btnValidate);
        mRegisterUserModel = RegisterUserModel.getInstance();
        mRegisterUserModel.addObserver(this);
        mRegisterUserModel.SendSMS();
    }

    public void onClickBtnValidate(View view) {

        EditText mLblValidCode = (EditText)findViewById(R.id.lblValidCode);
        mRegisterUserModel.ValidateUser(mLblValidCode.getText().toString());
    }

    public void update(Observable observable, Object reason) {
        String fReason = (String)reason;
        if (fReason.equals("mResultGetUserData") || fReason.equals("mResultUpdateUserName")) {
            Intent answerIntent = new Intent();
            if(mRegisterUserModel.mResultValidation.SERVER_RESPONSE.equals("ok")){

            }
            answerIntent.putExtra(KEY_VALIDATION_RESULT,mRegisterUserModel.mResultValidation.SERVER_RESPONSE);
            setResult(RESULT_OK, answerIntent);
            mRegisterUserModel.deleteObserver(this);
            finish();
        }

    }
}
