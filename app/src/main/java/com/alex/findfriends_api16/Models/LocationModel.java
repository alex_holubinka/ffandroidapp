package com.alex.findfriends_api16.Models;

import com.alex.findfriends_api16.Connections.CRequest;
import com.alex.findfriends_api16.Connections.CResult;
import com.alex.findfriends_api16.Connections.CServerConnection;
import com.alex.findfriends_api16.UserData.CUserTable;
import com.google.gson.reflect.TypeToken;

import java.util.List;
import java.util.Observable;

/**
 * Created by Alex on 30.10.2016.
 */

public class LocationModel  extends Observable {

    CResult mResultLocation;


    public void setUserLocation(double longitude, double latitude){
        CUserTable fUserTable = new CUserTable();
        fUserTable.mIdff_users = CUserTable.getInstance().mIdff_users;
        fUserTable.mLatitude = latitude;
        fUserTable.mLongitude = longitude;

        CRequest fRequest;
        fRequest = new CRequest();
        fRequest.mMethodName = "/setUserLocation";
        fRequest.ObjListRequest.add(fUserTable);
        CServerConnection fServerConnection = new CServerConnection(setUserLocationListener, fRequest);
        fServerConnection.mMethod = CServerConnection.FMethod.POST;
        fServerConnection.mResponseType = new TypeToken<List<CUserTable>>() {}.getType();
        fServerConnection.Exec();
    }

    CServerConnection.TaskListener setUserLocationListener = new CServerConnection.TaskListener() {
        @Override
        public void onFinished(CResult result){
            mResultLocation = result;
            if(mResultLocation.SERVER_RESPONSE.equals("ok")){

            }
            setChanged();
            notifyObservers("mResultUpdateUserName");
        }
    };
}
