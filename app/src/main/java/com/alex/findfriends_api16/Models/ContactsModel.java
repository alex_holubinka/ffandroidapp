package com.alex.findfriends_api16.Models;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;

import com.alex.findfriends_api16.Connections.CRequest;
import com.alex.findfriends_api16.Connections.CResult;
import com.alex.findfriends_api16.Connections.CServerConnection;
import com.alex.findfriends_api16.R;
import com.alex.findfriends_api16.UserData.CUserTable;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Alex on 18.09.2016.
 */

public class ContactsModel extends Observable {

    ArrayList<CUserTable> mContactList;
    Cursor cursor;
    int counter;
    private ProgressDialog pDialog;
    private Handler updateBarHandler;
    Context context;
   public ArrayList<String> mContactListView;




    public void getContacts(ContentResolver contentResolver)throws Exception {
        mContactList = new ArrayList<CUserTable>();
        CUserTable fUserTable;
        String phoneNumber = null;
        String email = null;
        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
        Uri EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
        String DATA = ContactsContract.CommonDataKinds.Email.DATA;
        StringBuffer output;
        //contentResolver = context.getContentResolver();
        cursor = contentResolver.query(CONTENT_URI, null, null, null, null);
        // Iterate every contact in the phone
        if (cursor.getCount() > 0) {
            counter = 0;
            while (cursor.moveToNext()) {
                String contact_id = cursor.getString(cursor.getColumnIndex(_ID));
                String name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));
                if (hasPhoneNumber > 0) {
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[]{contact_id}, null);
                    fUserTable = new CUserTable();
                    fUserTable.mName = name;
                    fUserTable.mContacts = new ArrayList<CUserTable>();
                    while (phoneCursor.moveToNext()) {
                        CUserTable contacts = new CUserTable();
                        contacts.mPhone = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                        fUserTable.mContacts.add(contacts);
                    }
                    mContactList.add(fUserTable);
                    phoneCursor.close();
                    // Read every email id associated with the contact
                    Cursor emailCursor = contentResolver.query(EmailCONTENT_URI, null, EmailCONTACT_ID + " = ?", new String[]{contact_id}, null);
                    while (emailCursor.moveToNext()) {

                    }
                    emailCursor.close();
                }
                // Add the contact to the ArrayList
                if (hasPhoneNumber != 0) {
                    // contactList.add(output.toString());
                }

            }
            cursor.close();

            CUserTable.getInstance().mContacts = mContactList; // TEMP
            setChanged();
            notifyObservers("getContacts");
        }

    }
    public void GetRegisteredContacts(){
        CRequest mCRequest = new CRequest();
        CUserTable fCUserTable = new CUserTable();
        //fCUserTable.mIdff_users = CUserTable.getInstance().mIdff_users;
        fCUserTable.mIdff_users = 1;
        fCUserTable.mContacts = new ArrayList<CUserTable>();
        fCUserTable.mContacts = mContactList;
        mCRequest.ObjListRequest.add(fCUserTable);
        mCRequest.mMethodName = "/getContacts";

        CServerConnection mCServerConnection = new CServerConnection(GetRegisteredContactsListener, mCRequest);
        mCServerConnection.mMethod = CServerConnection.FMethod.POST;
        mCServerConnection.mResponseType = new TypeToken<List<CUserTable>>() {}.getType();
        mCServerConnection.Exec();
    }
    public void GetContactListString(){
        StringBuffer output ;
        mContactListView = new ArrayList<String>();
        output = new StringBuffer();
        for (int i = 0; i < CUserTable.getInstance().mContacts.size(); i++) {
            output.append("\n" + R.string.phoneNumberUserName + " :" + CUserTable.getInstance().mContacts.get(i).mName);
                for (int j = 0; j < CUserTable.getInstance().mContacts.get(i).mContacts.size(); j++){
                    if(!CUserTable.getInstance().mContacts.get(i).mContacts.isEmpty()) {
                        output.append("\n " + R.string.phoneNumber + ":" + CUserTable.getInstance().mContacts.get(i).mContacts.get(j).mPhone);
                    }
                }

        }
        mContactListView.add(output.toString());
        setChanged();
        notifyObservers("GetContactListString");
    }

    CServerConnection.TaskListener GetRegisteredContactsListener = new CServerConnection.TaskListener() {
        @Override
        public void onFinished(CResult result){
            //if(result.SERVER_RESPONSE.equals("ok")){


            CUserTable fCUserTable = new CUserTable();
            boolean isFound = false;
            if (result == null) return;
            if (result.ObjListResult == null) return;
            if(!result.ObjListResult.isEmpty()){
                for(int i = 0; i < result.ObjListResult.size(); i++){
                    fCUserTable =(CUserTable) result.ObjListResult.get(i);
                    if(!CUserTable.getInstance().mContacts.isEmpty()) {
                        for (int j = 0; j < CUserTable.getInstance().mContacts.size(); j++) {
                            if(!CUserTable.getInstance().mContacts.isEmpty()) {
                                for (int n = 0; n < CUserTable.getInstance().mContacts.get(j).mContacts.size(); n++) {
                                    if (CUserTable.getInstance().mContacts.get(j).mContacts.get(n).mPhone.replace(" ", "").equals(fCUserTable.mPhone.replace(" ", ""))) {
                                        CUserTable.getInstance().mContacts.get(j).mIsFriend = fCUserTable.mIsFriend;
                                        CUserTable.getInstance().mContacts.get(j).mValidate = fCUserTable.mValidate;
                                        isFound = true;
                                        break;
                                    }
                                }
                            }
                        }
                        if (!isFound) {
                            CUserTable.getInstance().mContacts.add(fCUserTable);
                        }

                    }
                }
            }
            setChanged();
            notifyObservers("GetRegisteredContactsListener");
        }
    };
}