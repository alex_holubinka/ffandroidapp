package com.alex.findfriends_api16.Models;

import com.alex.findfriends_api16.Connections.CRequest;
import com.alex.findfriends_api16.Connections.CResult;
import com.alex.findfriends_api16.Connections.CServerConnection;
import com.alex.findfriends_api16.UserData.CUserTable;
import com.alex.findfriends_api16.UserData.CUserValidationData;
import com.google.gson.reflect.TypeToken;

import java.util.List;
import java.util.Observable;

/**
 * Created by Alex on 29.08.2016.
 */
public class ValidateRequestCodeModel extends Observable {
    public CResult mResult;

    public void SendSMS(){
        CRequest mCRequest = new CRequest();
        mCRequest.ObjListRequest.add(CUserTable.getInstance());
        mCRequest.mMethodName = "/sendSMS";
        CServerConnection mCServerConnection = new CServerConnection(SendSMSListener, mCRequest);
        mCServerConnection.mMethod = CServerConnection.FMethod.POST;
        mCServerConnection.Exec();
    }
    public void ValidateUser(String validcode){
        CUserValidationData fCUserValidationData= new CUserValidationData();
        fCUserValidationData.mValidcode = validcode;
        fCUserValidationData.mPhone = CUserTable.getInstance().mPhone; // треба подумати як зробити з номером
        CRequest mCRequest = new CRequest();
        mCRequest.mMethodName = "/validateClient";
        mCRequest.ObjListRequest.add(fCUserValidationData);
        CServerConnection mCServerConnection = new CServerConnection(ValidateRequestCodeListener, mCRequest);
        mCServerConnection.mMethod = CServerConnection.FMethod.POST;
        mCServerConnection.mResponseType = new TypeToken<List<CUserTable>>() {}.getType();
        // mCServerConnection.ValideteClient();
        mCServerConnection.Exec();
    }
    CServerConnection.TaskListener SendSMSListener = new CServerConnection.TaskListener() {
        @Override
        public void onFinished(CResult result){
            if(result.SERVER_RESPONSE.equals("ok")){

            }
        }
    };
    CServerConnection.TaskListener ValidateRequestCodeListener = new CServerConnection.TaskListener() {
        @Override
        public void onFinished(CResult result){
            mResult = result;
            setChanged();
            notifyObservers();
        }
    };
}
