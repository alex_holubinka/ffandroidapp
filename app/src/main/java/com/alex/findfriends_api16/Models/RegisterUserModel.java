package com.alex.findfriends_api16.Models;

import android.app.Activity;

import com.alex.findfriends_api16.Activity.RegisterUserActivity;
import com.alex.findfriends_api16.Connections.CRequest;
import com.alex.findfriends_api16.Connections.CResult;
import com.alex.findfriends_api16.Connections.CServerConnection;
import com.alex.findfriends_api16.UserData.CUserTable;
import com.alex.findfriends_api16.UserData.CUserValidationData;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * Created by Alex on 25.08.2016.
 */
public class RegisterUserModel extends Observable{

    private static RegisterUserModel instance;
    public CResult mResultRegister;
    public CResult mResultValidation;
    public CResult mResultSendSMS;
    public CResult mResultLogIn;
    public CResult mResultGetUserData;
    public CResult mResultUpdateUserName;
    public String mUserName;
    //public String mUswerPhone;

    public RegisterUserModel(){

    }

    public static RegisterUserModel getInstance() {
        if(instance == null){
            instance = new RegisterUserModel();
        }
        return instance;
    }

    public void auth(String userPhone, String userName,String passw){
        CRequest mCRequest = new CRequest();
        CUserTable.getInstance().mPhone = userPhone;
        CUserTable.getInstance().mName = userName;
        CUserTable.getInstance().mPassword = passw;
        CUserTable.getInstance().mValidate = false;
        mCRequest.ObjListRequest.add(CUserTable.getInstance());
        mCRequest.mMethodName = "/autentification";
        CServerConnection mCServerConnection = new CServerConnection(autentification, mCRequest);
        mCServerConnection.mMethod = CServerConnection.FMethod.POST;
        mCServerConnection.mResponseType = new TypeToken<List<CUserTable>>() {}.getType();
        mCServerConnection.Exec();


    }
    public void SendSMS(){
        CRequest mCRequest = new CRequest();
        mCRequest.ObjListRequest.add(CUserTable.getInstance());
        mCRequest.mMethodName = "/sendSMS";
        CServerConnection mCServerConnection = new CServerConnection(SendSMSListener, mCRequest);
        mCServerConnection.mMethod = CServerConnection.FMethod.POST;
        mCServerConnection.Exec();
    }
    public void ValidateUser(String validcode){
        CUserValidationData fCUserValidationData= new CUserValidationData();
        fCUserValidationData.mValidcode = validcode;
        fCUserValidationData.mPhone = CUserTable.getInstance().mPhone; // треба подумати як зробити з номером
        CRequest mCRequest = new CRequest();
        mCRequest.mMethodName = "/validateClient";
        mCRequest.ObjListRequest.add(fCUserValidationData);
        CServerConnection mCServerConnection = new CServerConnection(ValidateRequestCodeListener, mCRequest);
        mCServerConnection.mMethod = CServerConnection.FMethod.POST;
        mCServerConnection.mResponseType = new TypeToken<List<CUserTable>>() {}.getType();
        // mCServerConnection.ValideteClient();
        mCServerConnection.Exec();
    }
    private void LogIn(){
        CRequest mCRequest = new CRequest();
        CUserTable fCUserTable = CUserTable.getInstance();
        mCRequest.mMethodName = "/LogIn/";
        mCRequest.ObjListRequest.add(fCUserTable);
        CServerConnection mCServerConnection = new CServerConnection(LogInListener, mCRequest);
        mCServerConnection.mMethod = CServerConnection.FMethod.POST;
        mCServerConnection.Exec();
    }
    private void GetClientData(){
        CRequest mCRequest = new CRequest();
        mCRequest.mMethodName = "/GetClientData/";
        CServerConnection mCServerConnection = new CServerConnection(GetUserDataListener, mCRequest);
        mCServerConnection.mMethod = CServerConnection.FMethod.GET;
        mCServerConnection.mResponseType = new TypeToken<List<CUserTable>>() {}.getType();
        mCServerConnection.Exec();
    }
    public void UpdateUserName(){
        CRequest mCRequest = new CRequest();
        CUserTable fCUserTable = new CUserTable();
        fCUserTable.mName = mUserName;
        fCUserTable.mIdff_users = CUserTable.getInstance().mIdff_users;
        mCRequest.mMethodName = "/UpdateUserData/";
        mCRequest.ObjListRequest.add(fCUserTable);
        CServerConnection mCServerConnection = new CServerConnection(UpdateUserNameListener, mCRequest);
        mCServerConnection.mMethod = CServerConnection.FMethod.POST;
        mCServerConnection.Exec();

    }


    CServerConnection.TaskListener autentification = new CServerConnection.TaskListener() {
        @Override
        public void onFinished(CResult result){
            mResultRegister = result;
            setChanged();
            notifyObservers("mResultRegister");

        }
    };
    CServerConnection.TaskListener SendSMSListener = new CServerConnection.TaskListener() {
        @Override
        public void onFinished(CResult result){
            if(result.SERVER_RESPONSE.equals("ok")){
                mResultSendSMS = result;
            }
        }
    };
    CServerConnection.TaskListener ValidateRequestCodeListener = new CServerConnection.TaskListener() {
        @Override
        public void onFinished(CResult result){
            mResultValidation = result;
            if(mResultValidation.SERVER_RESPONSE.equals("ok")){
                LogIn();
            }else {
                setChanged();
                notifyObservers("mResultValidation");
            }
        }
    };
    CServerConnection.TaskListener LogInListener = new CServerConnection.TaskListener() {
        @Override
        public void onFinished(CResult result){
            mResultLogIn = result;
            if(mResultLogIn.SERVER_RESPONSE.equals("ok")){
                GetClientData();
            }else {
                setChanged();
                notifyObservers("mResultLogIn");
            }
        }
    };
    CServerConnection.TaskListener GetUserDataListener = new CServerConnection.TaskListener() {
        @Override
        public void onFinished(CResult result){
            mResultGetUserData = result;
            if(!mResultGetUserData.ObjListResult.isEmpty()){
                CUserTable fUserTable = CUserTable.getInstance();
                mUserName = fUserTable.mName;
                fUserTable.Copy((CUserTable) mResultGetUserData.ObjListResult.get(0));
                if(mResultRegister.SERVER_RESPONSE.equals("ok")){
                    //fUserTable.Copy((CUserTable) mResultGetUserData.ObjListResult.get(0));
                    setChanged();
                    notifyObservers("mResultGetUserData");
                }else if(mResultRegister.SERVER_RESPONSE.equals("user_duplicate")){
                    UpdateUserName();
                }


            }
        }
    };
    CServerConnection.TaskListener UpdateUserNameListener = new CServerConnection.TaskListener() {
        @Override
        public void onFinished(CResult result){
            mResultUpdateUserName = result;
            if(mResultUpdateUserName.SERVER_RESPONSE.equals("ok")){
                CUserTable fCUserTable = CUserTable.getInstance();
                fCUserTable.mName = mUserName;
            }
            setChanged();
            notifyObservers("mResultUpdateUserName");
        }
    };
}
